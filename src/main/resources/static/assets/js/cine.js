$('.cinema-seats .seat').on('click', function() {
  $(this).toggleClass('active');
});


function borrar() {
    swal({
        title: '¿Desea eliminar?',
        text: "Realmente desea eliminar",
        type: 'warning',
        showCancelButton: true,
        cancelButtonText: 'Cancelar',
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, deseo eliminar'
    }).then((result) => {
        if (result.value) {
            swal('Eliminado con exito!', 'Su archivo/dato fue eliminado correctamente', 'success')
        }
    })
}

function guardar() {
    swal({
        title: "¡Registro Exitoso!",
        text: "Se registro correctamente",
        type: "success",
        footer: 'Por favor ingrese con su usuario y contraseña',
        showConfirmButton: true,
        confirmButtonText: "Cerrar",
        closeOnConfirm: false
    }).then((result) => {
        if (result.value) {
            location.reload();

        }
    })
}

function activo() {
    swal({
        title: "Activación exitosa!",
        text: "La activación se realizo de manera correcta! Conserve su folio: 88F-2",
        type: "success",
        showConfirmButton: true,
        confirmButtonText: "Cerrar",
        closeOnConfirm: false
    }).then((result) => {
        if (result.value) {
            location.reload();
        }
    })
}

function warning() {
    swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
        if (result.value) {
            swal(
                'Deleted!',
                'Your file has been deleted.',
                'success'
            )
        }
    })
}