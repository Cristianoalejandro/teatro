package mx.edu.itsatlixco.teatro.exception;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * @author Carlos Montoya
 * @since 28/11/2018
 */
public class EmailExistsException extends Exception
{
	public EmailExistsException(@NotNull @NotEmpty String s)
	{
		super(s);
	}
}
