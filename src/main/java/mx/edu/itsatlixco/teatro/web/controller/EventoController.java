package mx.edu.itsatlixco.teatro.web.controller;

import lombok.extern.log4j.Log4j2;
import mx.edu.itsatlixco.teatro.service.EventoService;
import mx.edu.itsatlixco.teatro.web.dto.EventoDto;
import org.hibernate.annotations.Parameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.util.List;

@Log4j2
@Controller
@RequestMapping("/")
public class EventoController {

    private EventoService eventoService;

    @Autowired
    public EventoController(EventoService eventoService){
        this.eventoService = eventoService;
    }

    @GetMapping(value = "/evento")
    public String mostrarEvento(Model model) {
        refill(model);
        return "evento";
    }

    @PostMapping(value = "/eventoguardar")
    public String guardarEvento(Model model, @ModelAttribute("eventoobj") @Valid EventoDto eventoobj) {
        eventoService.guardarEvento(eventoobj);
        refill(model);
        return "evento";
    }




    /*@RequestMapping(value="/editar/{id}", method=RequestMethod.GET)
    public String editar(@PathVariable("id") long id, ModelMap mp){
        mp.put("usuario", uc.findOne(id));
        return "crud/editar";
    }

    @RequestMapping(value="/actualizar", method=RequestMethod.POST)
    public String actualizar(@Valid Usuario usuario, BindingResult bindingResult, ModelMap mp){
        if(bindingResult.hasErrors()){
            mp.put("usuarios", uc.findAll());
            return "crud/lista";
        }
        Usuario user = uc.findOne(usuario.getId());
        user.setNombre(usuario.getNombre());
        user.setPassword(usuario.getPassword());
        user.setEmail(usuario.getEmail());
        uc.save(user);
        mp.put("usuario", user);
        return "crud/actualizado";
    }*/

    private Model refill(Model model){
        model.addAttribute("guardado",true);
        model.addAttribute("eventoobj", new EventoDto());
        model.addAttribute("listaEvento",eventoService.listarEvento());
        model.addAttribute("host", ServletUriComponentsBuilder.fromCurrentContextPath().toUriString());
        return model;
    }
}

