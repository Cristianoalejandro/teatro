package mx.edu.itsatlixco.teatro.web.controller;

import lombok.extern.log4j.Log4j2;
import mx.edu.itsatlixco.teatro.db.entity.Usuario;
import mx.edu.itsatlixco.teatro.exception.EmailExistsException;
import mx.edu.itsatlixco.teatro.service.UsuarioService;
import mx.edu.itsatlixco.teatro.web.dto.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;


@Controller
@RequestMapping("/")
@Log4j2
public class RegisterController
{
	private final UsuarioService usuarioService;

	@Autowired
	public RegisterController(UsuarioService usuarioService)
	{
		this.usuarioService = usuarioService;
	}

	@RequestMapping(value = "/registro", method = RequestMethod.GET)
	public String mostrarFormaRegistro(Model model)
	{
		model.addAttribute("user", new UserDto());
		return "registration";
	}

	@RequestMapping(value = "/successRegister", method = RequestMethod.GET)
	public String mostrarRegistroExitoso()
	{
		return "successRegister";
	}



	@RequestMapping(value = "/registro", method = RequestMethod.POST)
	public ModelAndView registrarUsuario(
			@ModelAttribute("user") @Valid UserDto userDto,
			BindingResult result, WebRequest webRequest, Errors errors)
	{
		log.trace(userDto);
		Usuario usuario = new Usuario();

		if (!result.hasErrors())
			usuario = crearCuentaUsuario(userDto);

		if (usuario == null)
			result.rejectValue("email", "message.regError");

		if (result.hasErrors())
			return new ModelAndView("registro", "user", userDto);
		else
			return new ModelAndView("successRegister", "user", userDto);
	}

	private Usuario crearCuentaUsuario(UserDto userDto)
	{
		try
		{
			return usuarioService.registrarNuevoUsuario(userDto);
		}
		catch (EmailExistsException e) { return null; }
	}
}
