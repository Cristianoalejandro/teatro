package mx.edu.itsatlixco.teatro.web.controller;

import lombok.extern.log4j.Log4j2;
import mx.edu.itsatlixco.teatro.exception.EmailExistsException;
import mx.edu.itsatlixco.teatro.service.UsuarioService;
import mx.edu.itsatlixco.teatro.web.dto.UserDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.security.Principal;



@Log4j2
@Controller
@RequestMapping("/")
public class IndexController
{
	private UsuarioService usuarioService;

	public  IndexController (UsuarioService usuarioService){
		this.usuarioService = usuarioService;
	}

	@CrossOrigin
	@GetMapping(value = {"/","/index"})
	public String mostrarInicio(Model model,Principal principal)
	{
		model.addAttribute("host", ServletUriComponentsBuilder.fromCurrentContextPath().toUriString());
		model.addAttribute("mostrar", true);
		if (principal != null) {
			model.addAttribute("mostrar", false);
		}
		return "index";
	}

	@CrossOrigin
	@GetMapping("/login")
	public String login(Model model, Principal principal, RedirectAttributes redic){
		log.info("entre a login");
		model.addAttribute("mostrar", true);
		model.addAttribute("userobj",new UserDto());
		if (principal != null) {
			model.addAttribute("mostrar", false);
		}
		return "index";
	}

	@CrossOrigin
	@GetMapping(value="/logout")
	public String logoutPage (HttpServletRequest request, HttpServletResponse response,Model model) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null){
			new SecurityContextLogoutHandler().logout(request, response, auth);
		}
		model.addAttribute("userobj",new UserDto());
		model.addAttribute("mostrar", true);
		return "redirect:/index";
	}

	@CrossOrigin
	@PostMapping("/add_user")
	public ResponseEntity addUser(@Valid @RequestBody UserDto usuerobj){
		try {
			log.info(usuerobj.toString());
			usuarioService.registrarNuevoUsuario(usuerobj);
			return ResponseEntity.status(HttpStatus.CREATED).build();
		} catch (EmailExistsException e) {
			log.error(e);
			return ResponseEntity.status(HttpStatus.BAD_GATEWAY).build();
		}
	}


}
