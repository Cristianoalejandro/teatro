package mx.edu.itsatlixco.teatro.web.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;


@Data
public class EventoDto
{
	@NotNull
	@NotEmpty
	private String nombre;

	@NotNull
	private String fechaHora;

	private String lugar;
	private String urlImagen;
}
