package mx.edu.itsatlixco.teatro.web.security;

import lombok.extern.log4j.Log4j2;
import mx.edu.itsatlixco.teatro.db.entity.Privilegio;
import mx.edu.itsatlixco.teatro.db.entity.Rol;
import mx.edu.itsatlixco.teatro.db.entity.Usuario;
import mx.edu.itsatlixco.teatro.db.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;


@Service
@Log4j2
public class UserDetailsServiceImpl implements UserDetailsService
{
	private final UsuarioRepository usuarioRepository;

	@Autowired
	public UserDetailsServiceImpl(UsuarioRepository usuarioRepository)
	{
		this.usuarioRepository = usuarioRepository;
	}

	@Override
	@Transactional
	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException
	{
		Usuario usuario = usuarioRepository
				.findByEmail(username)
				.orElseThrow(() -> new UsernameNotFoundException(""));

		log.trace("Usuario encontrado: {}", usuario);

		return new User(usuario.getEmail(), usuario.getPassword(), true,
				true, true, true,
				getAuthorities(usuario.getRoles()));
	}

	private Collection<? extends GrantedAuthority> getAuthorities(Collection<Rol> roles)
	{
		List<Privilegio> privilegios = new ArrayList<>();
		roles.forEach(rol -> privilegios.addAll(rol.getPrivilegios()));

		return privilegios.stream()
				.map(p -> new SimpleGrantedAuthority(p.getNombre()))
				.collect(Collectors.toList());
	}
}
