package mx.edu.itsatlixco.teatro.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@Controller
@RequestMapping("/")
public class BoletoController {
    @RequestMapping(value = "/boleto", method = RequestMethod.GET)
    public String mostrarEvento(Model model){
        model.addAttribute("host", ServletUriComponentsBuilder.fromCurrentContextPath().toUriString());
        return "boletos";
    }
}
