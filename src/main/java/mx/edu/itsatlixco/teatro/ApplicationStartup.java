package mx.edu.itsatlixco.teatro;

import lombok.extern.log4j.Log4j2;
import mx.edu.itsatlixco.teatro.db.entity.Privilegio;
import mx.edu.itsatlixco.teatro.db.entity.Rol;
import mx.edu.itsatlixco.teatro.db.entity.Usuario;
import mx.edu.itsatlixco.teatro.db.repository.PrivilegioRepository;
import mx.edu.itsatlixco.teatro.db.repository.RolRepository;
import mx.edu.itsatlixco.teatro.db.repository.UsuarioRepository;
import mx.edu.itsatlixco.teatro.service.EventoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;


@Component
@Log4j2
public class ApplicationStartup implements ApplicationListener<ApplicationReadyEvent>
{
	private final UsuarioRepository usuarioRepository;
	private final RolRepository rolRepository;
	private final PrivilegioRepository privilegioRepository;
	private final PasswordEncoder passwordEncoder;

	@Autowired
	public ApplicationStartup(UsuarioRepository usuarioRepository, RolRepository rolRepository, PrivilegioRepository privilegioRepository, EventoService eventoService, PasswordEncoder passwordEncoder)
	{
		this.usuarioRepository = usuarioRepository;
		this.rolRepository = rolRepository;
		this.privilegioRepository = privilegioRepository;
		this.passwordEncoder = passwordEncoder;
	}

	/**
	 * This event is executed as late as conceivably possible to indicate that
	 * the application is ready to service requests.
	 */
	@Override
	@Transactional
	public void onApplicationEvent(final ApplicationReadyEvent event)
	{
		List<Privilegio> adminPrivilegios = new ArrayList<>();
		for (String p : new String[] {"registro", "compra"})
		{
			Privilegio privilegio = privilegioRepository.findByNombre(p);

			if (privilegio == null)
			{
				Privilegio nuevoPrivilegio = new Privilegio();
				nuevoPrivilegio.setNombre(p);
				privilegio = privilegioRepository.save(nuevoPrivilegio);
			}

			adminPrivilegios.add(privilegio);
		}

		Rol adminRol = rolRepository.findByNombre("admin");
		if (adminRol == null)
		{
			Rol nuevoAdminRol = new Rol();
			nuevoAdminRol.setNombre("admin");
			nuevoAdminRol.setPrivilegios(adminPrivilegios);
			adminRol = rolRepository.save(nuevoAdminRol);
		}

		Rol userRol = rolRepository.findByNombre("user");
		if (userRol == null)
		{
			userRol = new Rol();
			userRol.setNombre("user");
			userRol.setPrivilegios(adminPrivilegios.stream()
					.filter(p -> p.getNombre().equals("compra"))
					.collect(Collectors.toList()));
			rolRepository.save(userRol);
		}

		if (!usuarioRepository.existsByEmail("admin@itsatlixco.edu.mx"))
		{
			Usuario admin = new Usuario();
			admin.setNombre("admin");
			admin.setEmail("admin@itsatlixco.edu.mx");
			admin.setPassword(passwordEncoder.encode("admin"));
			admin.setRoles(Collections.singletonList(adminRol));
			usuarioRepository.save(admin);
		}

		log.info("Se han verificado los datos de administrador");

//		EventoDto eventoDto = new EventoDto();
//		eventoDto.setLugar("Atlixco");
//		eventoDto.setNombre("Evento de prueba");
//		eventoDto.setFechaHora(LocalDateTime.now());
//		eventoDto.setUrlImagen("http://www.google.com");
//
//		Evento evento = eventoService.guardarEvento(eventoDto);
//		log.info(evento);
//		log.info(evento.getFunciones());
	}
}
