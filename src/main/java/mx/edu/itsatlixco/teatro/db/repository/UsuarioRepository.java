package mx.edu.itsatlixco.teatro.db.repository;

import mx.edu.itsatlixco.teatro.db.entity.Usuario;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;


public interface UsuarioRepository extends CrudRepository<Usuario, Long>
{
	Optional<Usuario> findByEmail(String email);

	boolean existsByEmail(String email);
}
