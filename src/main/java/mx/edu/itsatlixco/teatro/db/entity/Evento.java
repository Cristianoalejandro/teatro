package mx.edu.itsatlixco.teatro.db.entity;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


@Entity
@Table(schema = "teatro")
@Data
public class Evento
{
	@Id
	@Column(name = "evento_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name = "evento_nombre", length = 200, nullable = false)
	private String nombre;

	@Column(name = "evento_lugar", length = 220, nullable = false)
	private String lugar;

	@Column(name = "evento_url_imagen")
	private String urlImagen;

	@OneToMany(mappedBy = "evento")
	@ToString.Exclude
	private List<Funcion> funciones = new ArrayList<>();
}
