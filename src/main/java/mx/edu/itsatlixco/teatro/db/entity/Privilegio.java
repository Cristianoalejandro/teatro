package mx.edu.itsatlixco.teatro.db.entity;

import lombok.Data;

import javax.persistence.*;


@Entity
@Table(schema = "teatro")
@Data
public class Privilegio
{
	@Id
	@Column(name = "privilegio_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name = "privilegio_nombre")
	private String nombre;
}
