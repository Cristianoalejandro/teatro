package mx.edu.itsatlixco.teatro.db.entity;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;


@Entity
@Table(schema = "teatro")
@Data
public class Rol
{
	@Id
	@Column(name = "rol_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name = "rol_nombre")
	private String nombre;

	@ManyToMany
	@JoinTable(name = "rol_privilegio", schema = "teatro",
			joinColumns = @JoinColumn(name = "rol_id"),
			inverseJoinColumns = @JoinColumn(name = "privilegio_id"))
	@ToString.Exclude
	private List<Privilegio> privilegios;
}
