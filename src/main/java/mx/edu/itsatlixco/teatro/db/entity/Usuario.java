package mx.edu.itsatlixco.teatro.db.entity;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;


@Entity
@Table(schema = "teatro")
@Data
public class Usuario
{
	@Id
	@Column(name = "usuario_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name = "usuario_nombre", length = 120, nullable = false)
	private String nombre;

	@Column(name = "usuario_apellido_paterno", length = 80)
	private String apellidoPaterno;

	@Column(name = "usuario_apellido_materno", length = 80)
	private String apellidoMaterno;

	@Column(name = "usuario_email", length = 150, nullable = false, unique = true, updatable = false)
	private String email;

	@Column(name = "usuario_password", length = 200, nullable = false)
	private String password;

	@ManyToMany
	@JoinTable(name = "usuario_rol", schema = "teatro",
			joinColumns = @JoinColumn(name = "usuario_id"),
			inverseJoinColumns = @JoinColumn(name = "rol_id"))
	@ToString.Exclude
	private List<Rol> roles;
}
