package mx.edu.itsatlixco.teatro.db.entity;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.time.LocalDateTime;


@Entity
@Table(schema = "teatro")
@Data
public class Funcion
{
	@Id
	@Column(name = "funcion_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "evento_id")
	@ToString.Exclude
	private Evento evento;

	@Column(name = "funcion_fecha_hora")
	private LocalDateTime fechaHora;
}
