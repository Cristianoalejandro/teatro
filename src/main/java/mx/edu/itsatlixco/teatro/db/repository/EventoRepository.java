package mx.edu.itsatlixco.teatro.db.repository;

import mx.edu.itsatlixco.teatro.db.entity.Evento;
import mx.edu.itsatlixco.teatro.db.entity.Funcion;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.UUID;


public interface EventoRepository extends CrudRepository<Evento, Long>
{
    @Query("select u from Evento u where u.id = :u ")
    Evento findOne(@Param("u") UUID u);
}
