package mx.edu.itsatlixco.teatro.db.repository;

import mx.edu.itsatlixco.teatro.db.entity.Privilegio;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;


public interface PrivilegioRepository extends CrudRepository<Privilegio, Integer>
{
	Privilegio findByNombre(String nombre);

	boolean existsByNombre(String nombre);
}
