package mx.edu.itsatlixco.teatro.db.repository;

import mx.edu.itsatlixco.teatro.db.entity.Boleto;
import org.springframework.data.repository.CrudRepository;


public interface BoletoRepository extends CrudRepository<Boleto, Integer>
{
}
