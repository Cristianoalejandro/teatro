package mx.edu.itsatlixco.teatro.db.repository;

import mx.edu.itsatlixco.teatro.db.entity.Funcion;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Set;
import java.util.UUID;


public interface FuncionRepository extends CrudRepository<Funcion, Integer>
{

}
