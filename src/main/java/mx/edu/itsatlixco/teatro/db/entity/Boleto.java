package mx.edu.itsatlixco.teatro.db.entity;

import lombok.Data;

import javax.persistence.*;


@Entity
@Table(schema = "teatro")
@Data
public class Boleto
{
	@Id
	@Column(name = "boleto_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "usuario_id")
	private Usuario usuario;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "funcion_id")
	private Funcion funcion;

	@Column(name = "boleto_fila", nullable = false)
	private int fila;

	@Column(name = "boleto_columna", nullable = false)
	private int columna;
}
