package mx.edu.itsatlixco.teatro.db.repository;

import mx.edu.itsatlixco.teatro.db.entity.Rol;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;


public interface RolRepository extends CrudRepository<Rol, Integer>
{
	Rol findByNombre(String nombre);

	boolean existsByNombre(String nombre);
}
