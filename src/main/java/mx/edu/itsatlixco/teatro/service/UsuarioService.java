package mx.edu.itsatlixco.teatro.service;

import mx.edu.itsatlixco.teatro.db.entity.Rol;
import mx.edu.itsatlixco.teatro.db.entity.Usuario;
import mx.edu.itsatlixco.teatro.db.repository.RolRepository;
import mx.edu.itsatlixco.teatro.db.repository.UsuarioRepository;
import mx.edu.itsatlixco.teatro.exception.EmailExistsException;
import mx.edu.itsatlixco.teatro.web.dto.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;


@Service
public class UsuarioService
{
	private final UsuarioRepository usuarioRepository;
	private final RolRepository rolRepository;
	private final PasswordEncoder passwordEncoder;

	@Autowired
	public UsuarioService(UsuarioRepository usuarioRepository, RolRepository rolRepository, PasswordEncoder passwordEncoder)
	{
		this.usuarioRepository = usuarioRepository;
		this.rolRepository = rolRepository;
		this.passwordEncoder = passwordEncoder;
	}

	@Transactional
	public Usuario registrarNuevoUsuario(UserDto userDto)
			throws EmailExistsException
	{
		if (usuarioRepository.existsByEmail(userDto.getEmail()))
			throw new EmailExistsException
					("Ya existe una cuenta con el email: " + userDto.getEmail());

		Usuario usuario = new Usuario();
		usuario.setNombre(userDto.getFirstName());
		usuario.setApellidoPaterno(userDto.getLastNameP());
		usuario.setApellidoMaterno(userDto.getLastNameM());
		usuario.setEmail(userDto.getEmail());
		usuario.setPassword(passwordEncoder.encode(userDto.getPassword()));

		Rol userRol = rolRepository.findByNombre("user");
		if (userRol != null) usuario.setRoles(Collections.singletonList(userRol));

		return usuarioRepository.save(usuario);
	}
}
