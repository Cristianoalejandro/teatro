package mx.edu.itsatlixco.teatro.service;

import mx.edu.itsatlixco.teatro.db.entity.Evento;
import mx.edu.itsatlixco.teatro.db.entity.Funcion;
import mx.edu.itsatlixco.teatro.db.repository.EventoRepository;
import mx.edu.itsatlixco.teatro.db.repository.FuncionRepository;
import mx.edu.itsatlixco.teatro.web.dto.EventoDto;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Service
public class EventoService
{
	private final EventoRepository eventoRepository;
	private final FuncionRepository funcionRepository;
	private final ModelMapper modelMapper;


	@Autowired
	public EventoService(EventoRepository eventoRepository, FuncionRepository funcionRepository, ModelMapper modelMapper)
	{
		this.eventoRepository = eventoRepository;
		this.funcionRepository = funcionRepository;
		this.modelMapper = modelMapper;
	}

	@Transactional
	public Evento guardarEvento(EventoDto eventoDto)
	{
		LocalDateTime dateTime = LocalDateTime.parse(eventoDto.getFechaHora(), DateTimeFormatter.ISO_DATE_TIME);

		Evento evento = eventoRepository.save(modelMapper.map(eventoDto, Evento.class));

		Funcion funcion = new Funcion();
		funcion.setEvento(evento);
		funcion.setFechaHora(dateTime);
		funcionRepository.save(funcion);

		evento.getFunciones().add(funcion);

		return evento;
	}

	public List<EventoDto> listarEvento(){
		List<EventoDto> ret = new ArrayList<>();
		eventoRepository.findAll().forEach(evento -> {
			EventoDto eventoE = new EventoDto();
			eventoE = modelMapper.map(evento, EventoDto.class);
			eventoE.setFechaHora(evento.getFunciones().get(0).getFechaHora().format(DateTimeFormatter.ISO_DATE_TIME));
			ret.add(eventoE);
		});
		return ret;
	}
}
