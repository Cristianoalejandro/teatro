package mx.edu.itsatlixco.teatro.service;

import mx.edu.itsatlixco.teatro.db.entity.Boleto;
import mx.edu.itsatlixco.teatro.db.entity.Funcion;
import mx.edu.itsatlixco.teatro.db.entity.Usuario;
import mx.edu.itsatlixco.teatro.db.repository.BoletoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BoletoService
{
	private final BoletoRepository boletoRepository;

	@Autowired
	public BoletoService(BoletoRepository boletoRepository)
	{
		this.boletoRepository = boletoRepository;
	}

	public Boleto comprarBoleto(Usuario usuario, Funcion funcion, int fila, int columna)
	{
		Boleto boleto = new Boleto();
		boleto.setUsuario(usuario);
		boleto.setFuncion(funcion);
		boleto.setFila(fila);
		boleto.setColumna(columna);
		return boletoRepository.save(boleto);
	}
}
