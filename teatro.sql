--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.8
-- Dumped by pg_dump version 9.6.8

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: teatro; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA teatro;


ALTER SCHEMA teatro OWNER TO postgres;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: hibernate_sequence; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.hibernate_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.hibernate_sequence OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: boleto; Type: TABLE; Schema: teatro; Owner: postgres
--

CREATE TABLE teatro.boleto (
    boleto_id integer NOT NULL,
    boleto_columna integer NOT NULL,
    boleto_fila integer NOT NULL,
    funcion_id integer,
    usuario_id integer
);


ALTER TABLE teatro.boleto OWNER TO postgres;

--
-- Name: boleto_boleto_id_seq; Type: SEQUENCE; Schema: teatro; Owner: postgres
--

CREATE SEQUENCE teatro.boleto_boleto_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE teatro.boleto_boleto_id_seq OWNER TO postgres;

--
-- Name: boleto_boleto_id_seq; Type: SEQUENCE OWNED BY; Schema: teatro; Owner: postgres
--

ALTER SEQUENCE teatro.boleto_boleto_id_seq OWNED BY teatro.boleto.boleto_id;


--
-- Name: evento; Type: TABLE; Schema: teatro; Owner: postgres
--

CREATE TABLE teatro.evento (
    evento_id integer NOT NULL,
    evento_lugar character varying(220) NOT NULL,
    evento_nombre character varying(200) NOT NULL,
    evento_url_imagen character varying(255)
);


ALTER TABLE teatro.evento OWNER TO postgres;

--
-- Name: evento_evento_id_seq; Type: SEQUENCE; Schema: teatro; Owner: postgres
--

CREATE SEQUENCE teatro.evento_evento_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE teatro.evento_evento_id_seq OWNER TO postgres;

--
-- Name: evento_evento_id_seq; Type: SEQUENCE OWNED BY; Schema: teatro; Owner: postgres
--

ALTER SEQUENCE teatro.evento_evento_id_seq OWNED BY teatro.evento.evento_id;


--
-- Name: funcion; Type: TABLE; Schema: teatro; Owner: postgres
--

CREATE TABLE teatro.funcion (
    funcion_id integer NOT NULL,
    funcion_fecha_hora timestamp without time zone,
    evento_id integer
);


ALTER TABLE teatro.funcion OWNER TO postgres;

--
-- Name: funcion_funcion_id_seq; Type: SEQUENCE; Schema: teatro; Owner: postgres
--

CREATE SEQUENCE teatro.funcion_funcion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE teatro.funcion_funcion_id_seq OWNER TO postgres;

--
-- Name: funcion_funcion_id_seq; Type: SEQUENCE OWNED BY; Schema: teatro; Owner: postgres
--

ALTER SEQUENCE teatro.funcion_funcion_id_seq OWNED BY teatro.funcion.funcion_id;


--
-- Name: privilegio; Type: TABLE; Schema: teatro; Owner: postgres
--

CREATE TABLE teatro.privilegio (
    privilegio_id integer NOT NULL,
    privilegio_nombre character varying(255)
);


ALTER TABLE teatro.privilegio OWNER TO postgres;

--
-- Name: privilegio_privilegio_id_seq; Type: SEQUENCE; Schema: teatro; Owner: postgres
--

CREATE SEQUENCE teatro.privilegio_privilegio_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE teatro.privilegio_privilegio_id_seq OWNER TO postgres;

--
-- Name: privilegio_privilegio_id_seq; Type: SEQUENCE OWNED BY; Schema: teatro; Owner: postgres
--

ALTER SEQUENCE teatro.privilegio_privilegio_id_seq OWNED BY teatro.privilegio.privilegio_id;


--
-- Name: rol; Type: TABLE; Schema: teatro; Owner: postgres
--

CREATE TABLE teatro.rol (
    rol_id integer NOT NULL,
    rol_nombre character varying(255)
);


ALTER TABLE teatro.rol OWNER TO postgres;

--
-- Name: rol_privilegio; Type: TABLE; Schema: teatro; Owner: postgres
--

CREATE TABLE teatro.rol_privilegio (
    rol_id integer NOT NULL,
    privilegio_id integer NOT NULL
);


ALTER TABLE teatro.rol_privilegio OWNER TO postgres;

--
-- Name: rol_rol_id_seq; Type: SEQUENCE; Schema: teatro; Owner: postgres
--

CREATE SEQUENCE teatro.rol_rol_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE teatro.rol_rol_id_seq OWNER TO postgres;

--
-- Name: rol_rol_id_seq; Type: SEQUENCE OWNED BY; Schema: teatro; Owner: postgres
--

ALTER SEQUENCE teatro.rol_rol_id_seq OWNED BY teatro.rol.rol_id;


--
-- Name: usuario; Type: TABLE; Schema: teatro; Owner: postgres
--

CREATE TABLE teatro.usuario (
    usuario_id integer NOT NULL,
    usuario_apellido_materno character varying(80),
    usuario_apellido_paterno character varying(80),
    usuario_email character varying(150) NOT NULL,
    usuario_nombre character varying(120) NOT NULL,
    usuario_password character varying(200) NOT NULL
);


ALTER TABLE teatro.usuario OWNER TO postgres;

--
-- Name: usuario_rol; Type: TABLE; Schema: teatro; Owner: postgres
--

CREATE TABLE teatro.usuario_rol (
    usuario_id integer NOT NULL,
    rol_id integer NOT NULL
);


ALTER TABLE teatro.usuario_rol OWNER TO postgres;

--
-- Name: usuario_usuario_id_seq; Type: SEQUENCE; Schema: teatro; Owner: postgres
--

CREATE SEQUENCE teatro.usuario_usuario_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE teatro.usuario_usuario_id_seq OWNER TO postgres;

--
-- Name: usuario_usuario_id_seq; Type: SEQUENCE OWNED BY; Schema: teatro; Owner: postgres
--

ALTER SEQUENCE teatro.usuario_usuario_id_seq OWNED BY teatro.usuario.usuario_id;


--
-- Name: boleto boleto_id; Type: DEFAULT; Schema: teatro; Owner: postgres
--

ALTER TABLE ONLY teatro.boleto ALTER COLUMN boleto_id SET DEFAULT nextval('teatro.boleto_boleto_id_seq'::regclass);


--
-- Name: evento evento_id; Type: DEFAULT; Schema: teatro; Owner: postgres
--

ALTER TABLE ONLY teatro.evento ALTER COLUMN evento_id SET DEFAULT nextval('teatro.evento_evento_id_seq'::regclass);


--
-- Name: funcion funcion_id; Type: DEFAULT; Schema: teatro; Owner: postgres
--

ALTER TABLE ONLY teatro.funcion ALTER COLUMN funcion_id SET DEFAULT nextval('teatro.funcion_funcion_id_seq'::regclass);


--
-- Name: privilegio privilegio_id; Type: DEFAULT; Schema: teatro; Owner: postgres
--

ALTER TABLE ONLY teatro.privilegio ALTER COLUMN privilegio_id SET DEFAULT nextval('teatro.privilegio_privilegio_id_seq'::regclass);


--
-- Name: rol rol_id; Type: DEFAULT; Schema: teatro; Owner: postgres
--

ALTER TABLE ONLY teatro.rol ALTER COLUMN rol_id SET DEFAULT nextval('teatro.rol_rol_id_seq'::regclass);


--
-- Name: usuario usuario_id; Type: DEFAULT; Schema: teatro; Owner: postgres
--

ALTER TABLE ONLY teatro.usuario ALTER COLUMN usuario_id SET DEFAULT nextval('teatro.usuario_usuario_id_seq'::regclass);


--
-- Name: hibernate_sequence; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.hibernate_sequence', 1, false);


--
-- Data for Name: boleto; Type: TABLE DATA; Schema: teatro; Owner: postgres
--

COPY teatro.boleto (boleto_id, boleto_columna, boleto_fila, funcion_id, usuario_id) FROM stdin;
\.


--
-- Name: boleto_boleto_id_seq; Type: SEQUENCE SET; Schema: teatro; Owner: postgres
--

SELECT pg_catalog.setval('teatro.boleto_boleto_id_seq', 1, false);


--
-- Data for Name: evento; Type: TABLE DATA; Schema: teatro; Owner: postgres
--

COPY teatro.evento (evento_id, evento_lugar, evento_nombre, evento_url_imagen) FROM stdin;
\.


--
-- Name: evento_evento_id_seq; Type: SEQUENCE SET; Schema: teatro; Owner: postgres
--

SELECT pg_catalog.setval('teatro.evento_evento_id_seq', 1, false);


--
-- Data for Name: funcion; Type: TABLE DATA; Schema: teatro; Owner: postgres
--

COPY teatro.funcion (funcion_id, funcion_fecha_hora, evento_id) FROM stdin;
\.


--
-- Name: funcion_funcion_id_seq; Type: SEQUENCE SET; Schema: teatro; Owner: postgres
--

SELECT pg_catalog.setval('teatro.funcion_funcion_id_seq', 1, false);


--
-- Data for Name: privilegio; Type: TABLE DATA; Schema: teatro; Owner: postgres
--

COPY teatro.privilegio (privilegio_id, privilegio_nombre) FROM stdin;
1	registro
2	compra
\.


--
-- Name: privilegio_privilegio_id_seq; Type: SEQUENCE SET; Schema: teatro; Owner: postgres
--

SELECT pg_catalog.setval('teatro.privilegio_privilegio_id_seq', 2, true);


--
-- Data for Name: rol; Type: TABLE DATA; Schema: teatro; Owner: postgres
--

COPY teatro.rol (rol_id, rol_nombre) FROM stdin;
1	admin
\.


--
-- Data for Name: rol_privilegio; Type: TABLE DATA; Schema: teatro; Owner: postgres
--

COPY teatro.rol_privilegio (rol_id, privilegio_id) FROM stdin;
1	1
1	2
\.


--
-- Name: rol_rol_id_seq; Type: SEQUENCE SET; Schema: teatro; Owner: postgres
--

SELECT pg_catalog.setval('teatro.rol_rol_id_seq', 1, true);


--
-- Data for Name: usuario; Type: TABLE DATA; Schema: teatro; Owner: postgres
--

COPY teatro.usuario (usuario_id, usuario_apellido_materno, usuario_apellido_paterno, usuario_email, usuario_nombre, usuario_password) FROM stdin;
1	\N	\N	admin@itsatlixco.edu.mx	admin	$2a$10$qT2N8sm8pYsEHUtOdZ1f5eIXCyuE64FtscJQCEIGVozgs4edk.RzO
\.


--
-- Data for Name: usuario_rol; Type: TABLE DATA; Schema: teatro; Owner: postgres
--

COPY teatro.usuario_rol (usuario_id, rol_id) FROM stdin;
1	1
\.


--
-- Name: usuario_usuario_id_seq; Type: SEQUENCE SET; Schema: teatro; Owner: postgres
--

SELECT pg_catalog.setval('teatro.usuario_usuario_id_seq', 1, true);


--
-- Name: boleto boleto_pkey; Type: CONSTRAINT; Schema: teatro; Owner: postgres
--

ALTER TABLE ONLY teatro.boleto
    ADD CONSTRAINT boleto_pkey PRIMARY KEY (boleto_id);


--
-- Name: evento evento_pkey; Type: CONSTRAINT; Schema: teatro; Owner: postgres
--

ALTER TABLE ONLY teatro.evento
    ADD CONSTRAINT evento_pkey PRIMARY KEY (evento_id);


--
-- Name: funcion funcion_pkey; Type: CONSTRAINT; Schema: teatro; Owner: postgres
--

ALTER TABLE ONLY teatro.funcion
    ADD CONSTRAINT funcion_pkey PRIMARY KEY (funcion_id);


--
-- Name: privilegio privilegio_pkey; Type: CONSTRAINT; Schema: teatro; Owner: postgres
--

ALTER TABLE ONLY teatro.privilegio
    ADD CONSTRAINT privilegio_pkey PRIMARY KEY (privilegio_id);


--
-- Name: rol rol_pkey; Type: CONSTRAINT; Schema: teatro; Owner: postgres
--

ALTER TABLE ONLY teatro.rol
    ADD CONSTRAINT rol_pkey PRIMARY KEY (rol_id);


--
-- Name: usuario uk_clitm05gie8jux1mv3wdkekqy; Type: CONSTRAINT; Schema: teatro; Owner: postgres
--

ALTER TABLE ONLY teatro.usuario
    ADD CONSTRAINT uk_clitm05gie8jux1mv3wdkekqy UNIQUE (usuario_email);


--
-- Name: usuario usuario_pkey; Type: CONSTRAINT; Schema: teatro; Owner: postgres
--

ALTER TABLE ONLY teatro.usuario
    ADD CONSTRAINT usuario_pkey PRIMARY KEY (usuario_id);


--
-- Name: usuario_rol fk610kvhkwcqk2pxeewur4l7bd1; Type: FK CONSTRAINT; Schema: teatro; Owner: postgres
--

ALTER TABLE ONLY teatro.usuario_rol
    ADD CONSTRAINT fk610kvhkwcqk2pxeewur4l7bd1 FOREIGN KEY (rol_id) REFERENCES teatro.rol(rol_id);


--
-- Name: usuario_rol fkbyfgloj439r9wr9smrms9u33r; Type: FK CONSTRAINT; Schema: teatro; Owner: postgres
--

ALTER TABLE ONLY teatro.usuario_rol
    ADD CONSTRAINT fkbyfgloj439r9wr9smrms9u33r FOREIGN KEY (usuario_id) REFERENCES teatro.usuario(usuario_id);


--
-- Name: rol_privilegio fkds9svnyhoao8f213paw349t6s; Type: FK CONSTRAINT; Schema: teatro; Owner: postgres
--

ALTER TABLE ONLY teatro.rol_privilegio
    ADD CONSTRAINT fkds9svnyhoao8f213paw349t6s FOREIGN KEY (privilegio_id) REFERENCES teatro.privilegio(privilegio_id);


--
-- Name: funcion fkekmiploy1lg8fcray8xrh2a4q; Type: FK CONSTRAINT; Schema: teatro; Owner: postgres
--

ALTER TABLE ONLY teatro.funcion
    ADD CONSTRAINT fkekmiploy1lg8fcray8xrh2a4q FOREIGN KEY (evento_id) REFERENCES teatro.evento(evento_id);


--
-- Name: rol_privilegio fkp0in4crwyhioirq6a6v3xag35; Type: FK CONSTRAINT; Schema: teatro; Owner: postgres
--

ALTER TABLE ONLY teatro.rol_privilegio
    ADD CONSTRAINT fkp0in4crwyhioirq6a6v3xag35 FOREIGN KEY (rol_id) REFERENCES teatro.rol(rol_id);


--
-- Name: boleto fkqj54t6xd8hq0edme8qsja0lwh; Type: FK CONSTRAINT; Schema: teatro; Owner: postgres
--

ALTER TABLE ONLY teatro.boleto
    ADD CONSTRAINT fkqj54t6xd8hq0edme8qsja0lwh FOREIGN KEY (funcion_id) REFERENCES teatro.funcion(funcion_id);


--
-- Name: boleto fkt7x0djycde3lawh2tosx9p2c5; Type: FK CONSTRAINT; Schema: teatro; Owner: postgres
--

ALTER TABLE ONLY teatro.boleto
    ADD CONSTRAINT fkt7x0djycde3lawh2tosx9p2c5 FOREIGN KEY (usuario_id) REFERENCES teatro.usuario(usuario_id);


--
-- PostgreSQL database dump complete
--

